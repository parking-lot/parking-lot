require 'rspec'
require './lib/vehicle'

describe 'Vehicle' do
  describe '#eql?' do
    it 'expects false for different vehicles' do
      expect(Vehicle.new(:KA_123)).to_not eql Vehicle.new(:KA_789)
    end

    it 'expects true for same vehicles' do
      expect(Vehicle.new(:KA_123)).to eql Vehicle.new(:KA_123)
    end

  end

end