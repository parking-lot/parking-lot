require 'rspec'
require './lib/parking_lot'
require './lib/vehicle'

describe 'ParkingLot' do
  describe '#is_slot_available' do
    it 'expects true when a new parking slot is available' do
      expect(ParkingLot.new(150).is_slot_available).to eq true
    end
    it 'expects false when a new parking slot is not available' do
      expect(ParkingLot.new(0).is_slot_available).to eq false
    end
  end
  describe '#park' do
    it 'expects not to raise error if slot is allocated' do
      new_parking_lot = ParkingLot.new(1)
      new_vehicle = Vehicle.new(:KA_123)
      expect {new_parking_lot.park(new_vehicle)}.to_not raise_error
    end
    it 'expects to raise error if slot is not allocated' do
      parked_vehicle = Vehicle.new(:KA_123)
      new_parking_lot = ParkingLot.new(1, [parked_vehicle])
      new_vehicle = Vehicle.new(:KA_789)
      expect {new_parking_lot.park(new_vehicle)}.to raise_error('No slot Available')
    end
    it 'expects to raise error if vechicle is already parked' do
      parked_vehicle = Vehicle.new(:KA_123)
      new_parking_lot = ParkingLot.new(5, [parked_vehicle])
      expect {new_parking_lot.park(parked_vehicle)}.to raise_error('Vehicle is already parked')
    end
  end

  describe '#un_park' do
    it 'expects to raise error if vehicle is not already parked' do
      new_parking_lot = ParkingLot.new(1)
      new_vehicle = Vehicle.new(:KA_123)
      expect {new_parking_lot.un_park(new_vehicle)}.to raise_error('Vehicle not parked')
    end
    it 'expects no error for unparking a parked vehicle' do
      parked_vehicle = Vehicle.new(:KA_123)
      new_parking_lot = ParkingLot.new(2, [parked_vehicle])
      expect {new_parking_lot.un_park(parked_vehicle)}.to_not raise_error
    end
    it 'expects to raise error if no slot available to park the vehicle' do
      parked_vehicle = Vehicle.new(:KA_123)
      new_parking_lot = ParkingLot.new(1, [parked_vehicle])
      new_vehicle = Vehicle.new(:KA_789)
      expect {new_parking_lot.un_park(new_vehicle)}.to raise_error('Vehicle not parked')
    end
  end

  describe '#parked?' do
    it 'expects true for a parked vehicle' do
      parked_vehicle = Vehicle.new(:KA_123)
      new_parking_lot = ParkingLot.new(1, [parked_vehicle])
      expect(new_parking_lot.parked?(parked_vehicle)).to eq true
    end
    it 'expects false for a unparked vehicle' do
      parked_vehicle = Vehicle.new(:KA_123)
      new_parking_lot = ParkingLot.new(1, [parked_vehicle])
      new_vehicle = Vehicle.new(:KA_789)
      expect(new_parking_lot.parked?(new_vehicle)).to eq false
    end
  end

  describe '#notified?' do
    let(:owner) {double('Person')}
    let(:traffic_cop) {double('Person')}

    it 'expects to notify owner and traffic cop when the parking lot becomes full' do

      #------------------------Arrange----------------------------------------------------------------#

      parked_vehicle = Vehicle.new(:KA_123)
      new_vehicle = Vehicle.new(:KA_789)
      new_parking_lot = ParkingLot.new(2, [parked_vehicle], [owner, traffic_cop])

      #------------------------Assert-----------------------------------------------------------------#

      expect(owner).to receive(:notify_lot_is_full)
      expect(traffic_cop).to receive(:notify_lot_is_full)

      #------------------------Action-----------------------------------------------------------------#

      new_parking_lot.park(new_vehicle)
    end

    it 'expects to notify owner and traffic cop if one slot becomes available' do
      parked_vehicle_1 = Vehicle.new(:KA_123)
      parked_vehicle_2 = Vehicle.new(:KA_567)
      new_parking_lot = ParkingLot.new(2, [parked_vehicle_1, parked_vehicle_2], [owner, traffic_cop])

      expect(owner).to receive(:notify_lot_is_available)
      expect(traffic_cop).to receive(:notify_lot_is_available)

      new_parking_lot.un_park(parked_vehicle_1)
    end
  end
end