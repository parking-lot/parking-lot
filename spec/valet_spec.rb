require './lib/valet'

require 'rspec'
describe 'Valet' do
  describe '#park' do
    it 'expects valet to park the car' do
      new_vehicle = Vehicle.new(:KA_897)
      parking_lot = ParkingLot.new(1)
      expect {Valet.new([parking_lot]).park(new_vehicle)}.to_not raise_error
    end
    it 'expects valet to unpark the car' do
      new_vehicle = Vehicle.new(:KA_123)
      parking_lot = ParkingLot.new(1, [new_vehicle])
      expect {Valet.new([parking_lot]).un_park(new_vehicle)}.to_not raise_error
    end
  end
  # describe '#check_and_park' do
  #   it 'expects the vehicle to be parked in a different lot if a lot is full' do
  #     parked_vehicle = Vehicle.new(:KA_567)
  #     new_vehicle = Vehicle.new(:KA_897)
  #     parking_lot_1 = ParkingLot.new(1, [parked_vehicle])
  #     parking_lot_2 = ParkingLot.new(2)
  #     expect {Valet.new([parking_lot_1, parking_lot_2]).park(new_vehicle)}.to_not raise_error
  #   end
  # end
  describe '#notify_full' do
    it 'expects to inform that the lots are full' do
      parked_vehicle_1 = Vehicle.new(:KA_567)
      parked_vehicle_2 = Vehicle.new(:KA_345)
      new_vehicle = Vehicle.new(:KA_897)
      parking_lot_1 = ParkingLot.new(1, [parked_vehicle_1])
      parking_lot_2 = ParkingLot.new(2, [parked_vehicle_2])
      expect {Valet.new([parking_lot_1,parking_lot_2]).notify_lot_is_full(parking_lot_1)}.to_not raise_error
    end
  end
end