SLOT_NUMBER = 0
#Represents the spots where vehicles can be left temporarily
class ParkingLot
  attr_accessor :is_full

  def initialize(total_slots, vehicles_parked = [], to_be_notified = [], is_full = false)
    @total_slots = total_slots
    @vehicles_parked = vehicles_parked
    @to_be_notified = to_be_notified
  end

  def is_slot_available
    @vehicles_parked.size < @total_slots
  end

  def park(vehicle)
    raise 'No slot Available' unless is_slot_available
    raise 'Vehicle is already parked' if parked?(vehicle)
    @vehicles_parked.push(vehicle)
    notify_if_lot_is_full unless is_slot_available
  end


  def un_park(vehicle)
    raise 'Vehicle not parked' unless parked?(vehicle)
    @vehicles_parked.delete(vehicle)
    notify_if_one_slot_available if has_one_empty_slot?
  end


  def parked?(vehicle)
    @vehicles_parked.include?(vehicle)
  end


  def has_one_empty_slot?
    @total_slots == @vehicles_parked.size + 1
  end

  #-------------------------------------NOTIFY---------------------------------------------------#
  def notify_if_lot_is_full
    @to_be_notified.each do |person|
      person.notify_lot_is_full(person)
    end
  end

  def notify_if_one_slot_available
    @to_be_notified.each do |person|
      person.notify_lot_is_available(person)
    end
  end

  def add_listener(person)
    @to_be_notified << person
    false unless is_slot_available
    true
  end

end