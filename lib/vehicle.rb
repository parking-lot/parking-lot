#Reprsents where the vehicle is parked
class Vehicle

  attr_reader :vehicle_number

  def initialize(vehicle_number)
    @vehicle_number = vehicle_number
  end

  def eql?(other)
    vehicle_number == other.vehicle_number
  end

  def hash
    vehicle_number.hash
  end
end