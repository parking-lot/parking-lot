class Valet
  attr_accessor :parking_lots, :available_lots

  def initialize(parking_lots)
    @parking_lots = parking_lots
    @available_lots = []
    register_as_listener
  end

  def park(vehicle)
    available_lots[0].park(vehicle)
  end


  def un_park(vehicle)
    parking_lots[0].un_park(vehicle)
  end

  def notify_lot_is_full(parking_lot)
    available_lots.delete(parking_lot)
  end

  def notify_lot_is_available(parking_lot)
    available_lots << parking_lot
  end

  def register_as_listener
    @parking_lots.each do |parking_lot|
      check = parking_lot.add_listener(self)
      if check
        available_lots << parking_lot
      end
    end
  end
end